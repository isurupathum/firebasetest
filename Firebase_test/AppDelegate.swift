//
//  AppDelegate.swift
//  Firebase_test
//
//  Created by vinoj randika on 9/15/20.
//  Copyright © 2020 vinoj randika. All rights reserved.
//

import UIKit
import Firebase
import UserNotifications
import Firebase
import FirebaseInstanceID
import FirebaseMessaging

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate//, MessagingDelegate
{

    var window: UIWindow?

    let gcmMessageIDKey = "gcm.message_id"//"message_id"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
         
        UNUserNotificationCenter.current().delegate = self
        Messaging.messaging().isAutoInitEnabled = true
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
         
        UNUserNotificationCenter.current().requestAuthorization(options: authOptions) { (_, error) in
            guard error == nil else{
                print(error!.localizedDescription)
                return
            }
        }
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
            }
        }
        //configureCategory()
        //triggerNotification()
        //requestAuth()
        
        application.registerForRemoteNotifications()
        return true
    }
    /*
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
           if let messageID = userInfo[gcmMessageIDKey] {
               print("Message ID: \(messageID)")
           }
        print("userinfo",userInfo)
        
    
    }*/
    
    
    // set for all massenger
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        print("UNNotificationResponse.self-->")
        
        //print(notification.request.content.body)
        completionHandler([.alert, .badge, .sound])
    }
  
    func userNotificationCenter(_ center: UNUserNotificationCenter,didReceive response: UNNotificationResponse,withCompletionHandler completionHandler:@escaping () -> Void) {
        print("UNUserNotificationCenter-->didReceive")
        let userInfo = response.notification.request.content.userInfo
        print(userInfo)
        let objcDict: NSDictionary = userInfo as NSDictionary
        let objcDictaps: NSDictionary = objcDict["aps"] as! NSDictionary
        //print(objcDict["aps"] as Any)
        //print("alert-->",objcDictaps["alert"])
        if let dict = userInfo as NSDictionary? as! [String:Any]? {
          print(dict["aps"])
        }
        print("request boday-->",response.notification.request.content.body)
        print("request sub title-->",response.notification.request.content.subtitle)
        print("request title-->",response.notification.request.content.title)
        print("request identifier-->",response.notification.request.identifier)
        if let dict = userInfo as NSDictionary? as! [String:Any]? {
          print(dict["Title"])
        }
        print(response.notification.request.content.categoryIdentifier)
           if response.notification.request.content.categoryIdentifier ==
                   "MEETING_INVITATION" {
           // Retrieve the meeting details.
           let meetingID = userInfo["MEETING_ID"] as! String
           let userID = userInfo["USER_ID"] as! String
                 
           switch response.actionIdentifier {
           case "ACCEPT_ACTION":
              print("ACCEPT_ACTION")

              break
                     
           case "DECLINE_ACTION":
            print("DECLINE_ACTION")
              break
                     
           case UNNotificationDefaultActionIdentifier,
                UNNotificationDismissActionIdentifier:
              // Queue meeting-related notifications for later
              //  if the user does not act.
              print("ELSE_ACTION")
              break
                     
           default:
              break
           }
        }
        else {
           // Handle other notification types...
        }
             
        // Always call the completion handler when done.
        completionHandler()
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
           print("Unable to register for remote notifications: \(error.localizedDescription)")
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        print("Firebase registration token: \(fcmToken)")
         
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
        // TODO: If necessary send token to application server.
        // Note: This callback is fired at each app startup and whenever a new token is generated.
    }
     
    private func application(application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: NSData) {
        Messaging.messaging().apnsToken = deviceToken as Data
    }
    
    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

    /*
    //set all masseger active status check
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
      let application = UIApplication.shared
      
      if(application.applicationState == .active){
        print("user tapped the notification bar when the app is in foreground")
        
      }
      
      if(application.applicationState == .inactive)
      {
        
        print("user tapped the notification bar when the app is in background")
        
        
      }
      
      /* Change root view controller to a specific viewcontroller */
      // let storyboard = UIStoryboard(name: "Main", bundle: nil)
      // let vc = storyboard.instantiateViewController(withIdentifier: "ViewControllerStoryboardID") as? ViewController
      // self.window?.rootViewController = vc
      
      completionHandler()
    }
*/
    
    /*
    private let category = "Notification.Category.Read"

    private let actionIdentifier = "Read"

    private func requestAuth() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) { (success, error) in
            if let error = error {
                print("Request Authorization Failed (\(error), \(error.localizedDescription))")
            }
        }
    }

    private func triggerNotification() {
        // Create Notification Content
        let notificationContent = UNMutableNotificationContent()

        // Configure Notification Content
        notificationContent.title = "Hello"
        notificationContent.body = "Kindly read this message."

        // Set Category Identifier
        notificationContent.categoryIdentifier = category

        // Add Trigger
        let notificationTrigger = UNTimeIntervalNotificationTrigger(timeInterval: 3.0, repeats: false)

        // Create Notification Request
        let notificationRequest = UNNotificationRequest(identifier: "test_local_notification", content: notificationContent, trigger: notificationTrigger)

        // Add Request to User Notification Center
        UNUserNotificationCenter.current().add(notificationRequest) { (error) in
            if let error = error {
                print("Unable to Add Notification Request (\(error), \(error.localizedDescription))")
            }
        }
    }

    private func configureCategory() {
        // Define Actions
        let read = UNNotificationAction(identifier: actionIdentifier, title: "Read", options: [])
        //let write = UNNotificationAction(identifier: actionIdentifier, title: "Write", options: [])

        // Define Category
        let readCategory = UNNotificationCategory(identifier: category, actions: [read], intentIdentifiers: [], options: [])
        

        // Register Category
        UNUserNotificationCenter.current().setNotificationCategories([readCategory])
        
    }


    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Void) {
        switch response.actionIdentifier {
        case actionIdentifier:
            print("Read tapped",actionIdentifier)
            
        default:
            print("Other Action")
        }

        
        completionHandler()
    }
*/
    
}

